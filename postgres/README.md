# Postgres

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: dev
```
```
  kubectl apply -f namespace.yaml
```
```yaml
kind: Service
apiVersion: v1
metadata:
  name: postgres-service
spec:
  selector:
    app: postgres
  ports:
  - name: sql
    port: 5432
    targetPort: 5432
```
```
  kubectl apply -f service.yaml
```
```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: postgres
  namespace: dev
spec:
  serviceName: postgres-service
  podManagementPolicy: Parallel
  replicas: 1
  selector:
    matchLabels:
      app: postgres
  template:
    metadata:
      labels:
        app: postgres
    spec:
      terminationGracePeriodSeconds: 10
      containers:
        - name: postgres
          image: postgres
          imagePullPolicy: Always
          ports:
          - name: sql
            containerPort: 5432
          env:
          - name: POSTGRES_USER
            value: admin
          - name: POSTGRES_PASSWORD
            value: admin
          - name: PGDATA
            value: /var/lib/postgresql/data
          volumeMounts:
          - name: postgres-pv-claim
            mountPath: /var/lib/postgresql/data
  volumeClaimTemplates:
  - metadata:
      name: postgres-pv-claim
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 1Gi 
```
```
  kubectl apply -f statefulset.yaml
```