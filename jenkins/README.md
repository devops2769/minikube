# Jenkins Deployment
## Crear el namespace
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: devops
```
```
  kubectl apply -f namespace.yaml
```
## Crear el StatefulSet
```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: jenkins
  namespace: devops
spec:
  serviceName: jenkins-service
  podManagementPolicy: Parallel
  replicas: 1
  updateStrategy:
    type: RollingUpdate
  selector:
    matchLabels:
      app: jenkins
  template:
    metadata:
      labels:
        app: jenkins
    spec:
      securityContext:
        fsGroup: 1000
        runAsUser: 0
      terminationGracePeriodSeconds: 10
      containers:
        - name: jenkins
          image: devopsmanuel/jenkins:jcasc.0.0.1
          imagePullPolicy: Always
          env:
          - name: JAVA_OPTS
            value: -Djenkins.install.runSetupWizard=false
          ports:
          - name: http
            containerPort: 8080
            protocol: TCP
          - containerPort: 5000
            protocol: TCP
          volumeMounts:
          - name: jenkins-pv-claim
            mountPath: /var/jenkins_home
  volumeClaimTemplates:
  - metadata:
      name: jenkins-pv-claim
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 1Gi 
```
```
  kubectl apply -f statefulset.yaml
```
## Crear el Service
```yaml
apiVersion: v1
kind: Service
metadata:
  name: jenkins-service
  namespace: devops
spec:
  selector:
    app: jenkins
  type: NodePort
  ports:
  - protocol: TCP
    port: 8080
    targetPort: http
```
```
  kubectl apply -f service.yaml
```
## Esponer el servicio en minikube
```
  minikube service jenkins-service -n devops -p cluster1
```