# minikube

## Practica minikue

Crear un cluster de dos nodos
```
  minikube start --nodes 2 -p cluster1
```

## Podemos consultar los nodos de nuestro cluster y el estado
```
  kubectl get nodes
```
```
  minikube status -p cluster1
```

## Desplegar hello world deployment
```
  kubectl apply -f hello-deployment.yaml
  kubectl rollout status deployment/hello

  kubectl apply -f hello-svc.yaml
```

```
  kubectl get pods -o wide
```

```
  minikube service hello -p cluster1
  # URL Tunnel
  curl  http://127.0.0.1:38417
```

...

Nota: este ejemplo practico se puede segir en [tutorial](https://minikube.sigs.k8s.io/docs/tutorials/multi_node/)