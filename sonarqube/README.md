# Sonarquebe
## Crear Namespace
```yaml
# namespace.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: sonar
```
```bash
  kubectl apply -f namespace.yaml
```
## Crear PVC

```yaml
# postgrest-pvc.yaml
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: postgres-pvc
  namespace: sonar
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 2Gi
```
```bash
  kubectl apply -f postgrest-pvc.yaml
```
## deployment postgres
```yaml
# postgrest-deployment.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: postgres
  namespace: sonar
  labels:
    app: postgres
data:
  POSTGRESQL_USERNAME: sonar_user
  POSTGRESQL_DATABASE: sonar_db
  POSTGRESQL_PASSWORD: S0N4RQUB3
---
apiVersion: v1
kind: Service
metadata:
  name: postgres
  namespace: sonar
  labels:
    app: postgres
spec:
  ports:
  - port: 5432
    name: postgres
  selector:
    app: postgres
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: postgres
  namespace: sonar
  labels:
    app: postgres
spec:
  replicas: 1
  selector:
    matchLabels:
      app: postgres
  template:
    metadata:
      labels:
        app: postgres
    spec:
      securityContext:
        fsGroup: 1000
        runAsUser: 0 
      containers:
      - name: postgress
        image: bitnami/postgresql
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 5432
        env:
        - name: ALLOW_EMPTY_PASSWORD
          value: "yes"
        envFrom:
        - configMapRef:
            name: postgres
        volumeMounts:
        - name: postgres
          mountPath: /bitnami/postgresql
      volumes:
      - name: postgres
        persistentVolumeClaim:
          claimName: postgres-pvc
```
```bash
 kubectl apply -f postgrest-deployment.yaml
```
## Create deployment
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: sonar-config
  namespace: sonar
  labels:
    app: sonar
data:
  SONARQUBE_JDBC_URL: "jdbc:postgresql://postgres:5432/sonar_db"
  SONARQUBE_JDBC_USERNAME: "sonar_user"
  SONARQUBE_JDBC_PASSWORD: "S0N4RQUB3"
  JAVA_OPTS: "-Duser.timezone=Asia/Jakarta -Xmx2048m"
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: sonar
  namespace: sonar
  labels:
    app: sonar
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: sonar
  template:
    metadata:
      labels:
        app: sonar
    spec:
      securityContext:
        fsGroup: 1000
        runAsUser: 0 
      initContainers:
      - name: init
        image: busybox
        command:
        - sysctl
        - -w
        - vm.max_map_count=262144
        imagePullPolicy: IfNotPresent
        securityContext:
          privileged: true
      containers:
      - name: sonarqube
        image: sonarqube:8.9.1-community
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 9000
        envFrom:
        - configMapRef:
            name: sonar-config
        volumeMounts:
        - name: app-pvc
          mountPath: "/opt/sonarqube/data/"
          subPath: data
        - name: app-pvc
          mountPath: "/opt/sonarqube/extensions/"
          subPath: extensions
        resources:
          requests:
            memory: "1024Mi"
          limits:
            memory: "2048Mi"
      volumes:
      - name: app-pvc
        persistentVolumeClaim:
          claimName: sonar-pvc
```
```bash
  kubectl apply -f sonarqube-deployment.yaml
```
## Create sonar pvc
```yaml
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: sonar-pvc
  namespace: sonar
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 2Gi
```
```bash
  kubectl apply -f sonar-pvc.yaml
```
## Crear Ingress
```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: sonar
  namespace: kube-system
spec:
  ingressClassName: nginx
  rules:
    - http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: sonar
                port:
                  number: 9000
```
```bash
  kubectl apply -f ingress.yaml
```