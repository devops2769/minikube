# Creamos el Secret
```
  kubectl apply -f pgadmin-secret.yaml
```

# crear el configmap
```
  kubectl apply -f pgadmin-configmap.yaml
```

# Service
```
  kubectl apply -f pgadmin-service.yaml
```

# Service
```
  kubectl apply -f pgadmin-statefulset.yaml
```

# Exponer servicio
```
  minikube service pgadmin-service --url
```
  o
```
  kubectl port-forward service/pgadmin-service 8080:80
```

# datos de inicio
 - username: user@domain.com
 - password: SuperSecret

 # Guia

 [pgadmin-kubernetes](https://www.enterprisedb.com/blog/how-deploy-pgadmin-kubernetes)