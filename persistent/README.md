# Practica Storage

```yaml
# pv0001.yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv0001
spec:
  accessModes:
    - ReadWriteOnce
  capacity:
    storage: 5Gi
  hostPath:
    path: /data/pv0001/
```
```bash
  kubectl apply -f pv0001.yaml
```
## Consultar el PV
```bash
  kubectl get pv
```
## Ver más información de PV
```bash
  kubectl describe pv pv0001
```
## Crear el PVC
```yaml
# test-pvc.yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: test-pvc
  namespace: default
  labels:
    app: test-pvc
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 2Gi
```
```bash
  kubectl apply -f test-pvc.yaml
```
```bash
  kubectl get pvc
```
## Crear Pod
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: mypod
  labels:
    app: mypod
spec:
  containers:
  - name: myfrontend
    image: nginx
    resources:
      limits:
        cpu: 200m
        memory: 500Mi
      requests:
        cpu: 100m
        memory: 200Mi
    volumeMounts:
    - name: mypod
      mountPath: /var/www/html
  volumes:
    - name: mypod
      persistentVolumeClaim:
        claimName: test-pvc
```
```bash
  kubectl apply -f pod.yaml
```

