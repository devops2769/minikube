# PersistentVolume
```yaml
# persistent-volume.yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv001
spec:
  accessModes:
  - ReadWriteOnce
  capacity:
    storage: 1Gi
  hostPath:
    path: /data/pv001/
```
## Crear PersistenVolume
```
  kubectl apply -f persistent-volume.yaml
```
```yaml
# persistent-volume-claim.yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: jenkins-pv-claim
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```
## Crear PersistenVolumeClaim
```
  kubectl apply -f persistent-volume-claim.yaml
```